import 'dart:async';

import 'package:restaurant_finder/DataLayer/Restaurant.dart';

import 'Bloc.dart';

class FavouriteBloc implements Bloc {
  var _restaurants = <Restaurant>[];
  List<Restaurant> get favourites => _restaurants;

  final _controller = StreamController<List<Restaurant>>.broadcast();
  Stream<List<Restaurant>> get favouritesStream => _controller.stream;

  void toggleRestaurant(Restaurant restaurant) {
    if (_restaurants.contains(restaurant)) {
      _restaurants.remove(restaurant);
    } else {
      _restaurants.add(restaurant);
    }

    _controller.sink.add(_restaurants);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
