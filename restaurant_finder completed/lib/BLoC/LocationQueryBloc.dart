import 'dart:async';

import 'package:restaurant_finder/BLoC/Bloc.dart';
import 'package:restaurant_finder/DataLayer/Location.dart';
import 'package:restaurant_finder/DataLayer/ZomatoClient.dart';

class LocationQueryBloc implements Bloc {
  final _controller = StreamController<List<Location>>();
  final _client = ZomatoClient();
  Stream<List<Location>> get locationStream => _controller.stream;

  void submitQuery(String query) async {
    final results = await _client.fetchLocations(query);
    if (_controller.isClosed) {
      return;
    }

    _controller.sink.add(results);
  }

  @override
  void dispose() {
    _controller.close();
  }
}
