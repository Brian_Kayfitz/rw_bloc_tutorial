import 'package:flutter/material.dart';
import 'package:restaurant_finder/BLoC/BlocProvider.dart';
import 'package:restaurant_finder/BLoC/FavouriteBloc.dart';
import 'package:restaurant_finder/DataLayer/Restaurant.dart';
import 'package:restaurant_finder/UI/RestaurantTile.dart';

class FavouriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<FavouriteBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Favourites'),
      ),
      body: StreamBuilder<List<Restaurant>>(
        stream: bloc.favouritesStream,
        initialData: bloc.favourites,
        builder: (context, snapshot) {
          List<Restaurant> favourites =
              (snapshot.connectionState == ConnectionState.waiting)
                  ? bloc.favourites
                  : snapshot.data;

          if (favourites == null || favourites.isEmpty) {
            return Center(child: Text('No Favourites'));
          }

          return ListView.separated(
            itemCount: favourites.length,
            separatorBuilder: (context, index) => Divider(),
            itemBuilder: (context, index) {
              final restaurant = favourites[index];
              return RestaurantTile(restaurant: restaurant);
            },
          );
        },
      ),
    );
  }
}
