import 'package:flutter/material.dart';
import 'package:restaurant_finder/BLoC/BlocProvider.dart';
import 'package:restaurant_finder/BLoC/LocationBloc.dart';
import 'package:restaurant_finder/DataLayer/Location.dart';
import 'package:restaurant_finder/UI/LocationScreen.dart';
import 'package:restaurant_finder/UI/RestaurantScreen.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Location>(
        stream: BlocProvider.of<LocationBloc>(context).locationStream,
        builder: (context, snapshot) {
          final location = snapshot.data;

          if (location == null) {
            return LocationScreen();
          } else {
            return RestaurantScreen(location: location);
          }
        });
  }
}
