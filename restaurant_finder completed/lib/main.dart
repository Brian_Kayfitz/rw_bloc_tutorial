import 'package:flutter/material.dart';
import 'package:restaurant_finder/BLoC/BlocProvider.dart';
import 'package:restaurant_finder/BLoC/FavouriteBloc.dart';
import 'package:restaurant_finder/UI/MainScreen.dart';

import 'BLoC/LocationBloc.dart';

void main() => runApp(RestaurantFinder());

class RestaurantFinder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LocationBloc>(
      bloc: LocationBloc(),
      child: BlocProvider<FavouriteBloc>(
        bloc: FavouriteBloc(),
        child: MaterialApp(
          title: 'Restaurant Finder',
          theme: ThemeData(
            primarySwatch: Colors.red,
          ),
          home: MainScreen(),
        ),
      ),
    );
  }
}
